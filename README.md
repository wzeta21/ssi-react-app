# REACT PROJECT

This project was created to practice react, to improve my skills as a software developer. Also this is my first adventure with react

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

Clone the project, in the project directory, you can run:

### `yarn install`

After that, run:

### `yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.
