import * as ActionTypes from "./ActionTypes";

export const Employees = (
    state = {
        isLoading: true,
        errMess: null,
        empleados: []
    },
    action) => {
    switch (action.type) {
        case ActionTypes.ADD_EMPLOYEES:
            return {...state, isLoading: false, errMess: null, empleados: action.payload};
        case ActionTypes.EMPLOYEES_LOADING:
            return {...state, isLoading: true, errMess: null, empleados: []};
        case ActionTypes.EMPLOYEES_FAILED:
            return {...state, isLoading: false, errMess: action.payload, empleados: []};
        default:
            return state;
    }
};
